#include"Date.h"

Date::Date() {
    time_t timer;
    struct tm *ptm;
    time(&timer);// получаем текущее время время
    ptm = gmtime(&timer);// переводим его в часовой пояс UTC и заполняем структуру tm
    years = static_cast<unsigned> ( ptm->tm_year + 1900);
    currentMonths = convertToEnum(static_cast<unsigned>(ptm->tm_mon + 1));
    days = static_cast<unsigned> (ptm->tm_mday);
    hours = static_cast<unsigned> (ptm->tm_hour);
    minutes = static_cast<unsigned> (ptm->tm_min);
    seconds = static_cast<unsigned> (ptm->tm_sec);
}

Date::Date(const Date &obj) {
    *this = obj;
}

Date::Date(unsigned int year, Month month, unsigned int day, unsigned int hour, unsigned int minute,
           unsigned int second)  {
    years = year;
    currentMonths = month;
    days = day;
    hours = hour;
    minutes = minute;
    seconds = second;

    NormalizationOfDate();
}

Date::Date(unsigned int year, Month month, unsigned int day):  Date(year, month, day, 0, 0, 0){

}

Date::Date(unsigned int hrs, unsigned int min, unsigned int sec) {
    time_t timer;
    struct tm *ptm;
    time(&timer);
    ptm = gmtime(&timer);
    *this=Date((unsigned) ptm->tm_year + 1900,(Month)ptm->tm_mon,(unsigned) ptm->tm_mday + 1,hrs,min,sec);
}

Date Date::addYears(int years) const{
    Date date(this->years + years, currentMonths, days, hours, minutes, seconds);
    return date;
}

Date Date::addMonths(int months) const{
    Date date(years, convertToEnum(convertFromEnum(this->currentMonths) + months), days, hours,
              minutes, seconds);
    return date;
}
Date Date::addDays(int days) const{
    Date date(years, currentMonths, this->days+days, hours, minutes, seconds);
    return date;
}

Date Date::addHours(int hours) const{
    Date date(years, currentMonths, days, this->hours+hours, minutes, seconds);
    return date;
}

Date Date::addMinutes(int minutes) const{
    Date date(years, currentMonths, days, hours, this->minutes+minutes, seconds);
    return date;
}

Date Date::addSeconds(int seconds) const{
    Date date(years, currentMonths, days, hours, minutes, this->seconds + seconds);
    return date;
}

Date Date::NormalizationOfDate() {
    unsigned int mon = currentMonths;
    minutes = minutes + (seconds / 60);
    seconds = seconds % 60;

    hours = hours + (minutes / 60);//reinterpret_cast
    minutes = minutes % 60;

    days = days + (hours / 24);
    hours = hours % 24;

    while (days > months[mon - 1]) {
        if (mon == Feb) {
            if (__isleap(years)) {
                days = days - months[mon - 1] - 1;
                mon++;
                continue;
            }
        }
        days = days - months[mon - 1];
        mon++;
        if (mon > Dec) {
            years++;
            mon = Jan;
        }
    }
    currentMonths= convertToEnum(mon);
    try {
        if (years > 9999) {
            throw std::invalid_argument("incorrect value");
        }
    }

    catch (const std::invalid_argument &ia) {
        Date date(9999, Dec, 31, 23, 59, 59);
        *this=date;// почему нельзя return
    }

    try {
        if (years < 1) {
            throw std::invalid_argument("incorrect value");
        }
    }
    catch (const std::invalid_argument &ia) {
        Date date(1, Jan, 1, 0, 0, 0);
        *this=date;
    }
}

unsigned int Date::getYears() const{
    return years;
}

unsigned int Date::getMonths() const{
    return currentMonths;
}

unsigned int Date::getDays() const{
    return days;
}

unsigned int Date::getHours() const{
    return hours;
}

unsigned int Date::getMinutes() const{
    return minutes;
}

unsigned int Date::getSeconds() const{
    return seconds;
}


unsigned int Date::convertFromEnum(Month month) {
    return (unsigned)month;
}

Month Date::convertToEnum(unsigned int intMon) {
    switch (intMon) {
        case 1:
            return Jan;
        case 2:
            return Feb;
        case 3:
            return Mar;
        case 4:
            return Apr;
        case 5:
            return May;
        case 6:
            return Jun;
        case 7:
            return Jul;
        case 8:
            return Aug;
        case 9:
            return Sep;
        case 10:
            return Oct;
        case 11:
            return Nov;
        case 12:
            return Dec;
        default:
            return convertToEnum(intMon % 12);
    }

}

DateInterval Date::getInterval(const Date &another) const {

    DateInterval dateInterval;
    int check = 0;
    int YearsInterval = another.years - this->years;
    int MonthsInterval = another.currentMonths - this->currentMonths;
    int DaysInterval = another.days - this->days;
    int HoursInterval = another.hours - this->hours;
    int MinutesInterval = another.minutes - this->minutes;
    int SecondsInterval = another.seconds - this->seconds;
    if (YearsInterval < 0) {
        check = 1;
        YearsInterval = YearsInterval * (-1);
        MonthsInterval = MonthsInterval * (-1);
        DaysInterval = DaysInterval * (-1);
        HoursInterval = HoursInterval * (-1);
        MinutesInterval = MinutesInterval * (-1);
        SecondsInterval = SecondsInterval * (-1);
    }
    // Нормализуем интервал, занимая единицу из разряда выше
    // Также нужно от большего года отнимать меньший, иначе возможно
    // беру дни из текущего месца( даты которая отнимается)
    if (SecondsInterval < 0) {
        MinutesInterval--;
        SecondsInterval = SecondsInterval + 60;
    }
    if (MinutesInterval < 0) {
        HoursInterval--;
        MinutesInterval = MinutesInterval + 60;
    }
    if (HoursInterval < 0) {
        DaysInterval--;
        HoursInterval = HoursInterval + 24;
    }
    if ((DaysInterval < 0) && (check == 0)) {
        MonthsInterval--;
        DaysInterval = DaysInterval + months[this->currentMonths - 1];
        if ((this->currentMonths) == Feb) {
            if (__isleap(this->years + (this->currentMonths) / 12))
                DaysInterval++;
        }
    } else if ((DaysInterval < 0) && (check == 1)) {
        MonthsInterval--;
        DaysInterval = DaysInterval + months[another.currentMonths - 1];
        if ((another.currentMonths) == Feb) {
            if (__isleap(another.years + (another.currentMonths) / 12))
                DaysInterval++;
        }
    }
    if (MonthsInterval < 0) {
        YearsInterval--;
        MonthsInterval = MonthsInterval + 12;
    }
    // загружаем данные в объект
    dateInterval.setYearsInterval(YearsInterval);
    dateInterval.setMonthsInterval(MonthsInterval);
    dateInterval.setDaysInterval(DaysInterval);
    dateInterval.setHoursInterval(HoursInterval);
    dateInterval.setMinutesInterval(MinutesInterval);
    dateInterval.setSecondsInterval(SecondsInterval);
    return dateInterval;
}

Date Date::addInterval(const DateInterval &another) const { // учет того, что параметры в могут быть отрицательны
    int tmpYear = this->years + another.getYearsInterval();
    int tmpMonth = convertFromEnum(this->currentMonths) + another.getMonthsInterval();
    int tmpDay = this->days + another.getDaysInterval();
    int tmpHour = this->hours + another.getHoursInterval();
    int tmpMinute = this->minutes + another.getMinutesInterval();
    int tmpSeconds = this->seconds + another.getSecondsInterval();

    if (tmpSeconds < 0) {
        tmpMinute = tmpMinute + tmpSeconds / 60;
        tmpSeconds = tmpSeconds % 60;
        if (tmpSeconds != 0) {// занимаем минуту
            tmpMinute--;
        }
    }

    if (tmpMinute < 0) {
        tmpHour = tmpHour + tmpMinute / 60;
        tmpMinute = tmpMinute % 60;
        if (tmpMinute != 0) {// занимаем час
            tmpHour--;
        }
    }

    if (tmpHour < 0) {
        tmpDay = tmpDay + tmpHour / 24;
        tmpHour = tmpHour % 24;

        if (tmpHour != 0) {
            tmpDay--;
        }
    }
    //Если мы используем массив, то мы должны отнимать 1, чтобы начиналось с нуля
    if (tmpDay < 0) {
        int i = this->currentMonths - 2;// отнимаем мсяцы -2 потому что с нуля массив(-1) и сразу -1 отняли
        while ((int) (months[i] + tmpDay) < 0) {
            tmpDay = tmpDay + months[i];
            tmpMonth--;
            if ((__isleap(this->years)) && (i == Feb)) {
                tmpDay++;
            }
            if (i == 0) {
                tmpYear--;
                tmpMonth = tmpMonth + 12;
            }
            i--;
            if (i <= -1) {// если переходит на год назад
                i = 11;
            }
        }
        tmpDay = tmpDay + months[i];
        if ((__isleap(this->years)) && (i == Feb)) {// вдруг пр выходе этот i отвечает за Февраль
            tmpDay++;
        }
        tmpMonth--;
    }
    if (tmpMonth < 0) {
        tmpYear = tmpYear + tmpMonth / 12;
        tmpMonth = tmpMonth + 12;
        if (tmpMonth != 0) {
            tmpYear--;
        }
    }
    Date date(static_cast<unsigned>(tmpYear), convertToEnum(static_cast<unsigned>(tmpMonth)),
              static_cast<unsigned>(tmpDay),
              static_cast<unsigned>(tmpHour), static_cast<unsigned>(tmpMinute), static_cast<unsigned>(tmpSeconds));

    return date;
}

std::string Date::formatDate(std::string format) {
    std::string Year = "YYYY";
    std::string MonthN = "MM";
    std::string MonthL = "MMM";
    std::string Day = "DD";
    std::string Hour = "hh";
    std::string Minute = "mm";
    std::string Seconds = "ss";

    if ((format.find(Year, 0) == std::string::npos) && (format.find(MonthN, 0) == std::string::npos) &&
            (format.find(Day, 0) == std::string::npos) && (format.find(Hour, 0) == std::string::npos) &&
            (format.find(Minute, 0) == std::string::npos) && (format.find(Seconds, 0) == std::string::npos)) {
            return "Invalid date formate";

    }
    //Важный момент: MonthL перед MonthN, потому что она больше, и не будет неправильной замены
    boost::replace_all(format, Year, std::to_string(years));
    boost::replace_all(format, MonthL, Convert[currentMonths]);
    boost::replace_all(format, MonthN, std::to_string(currentMonths));
    boost::replace_all(format, Day, std::to_string(days));
    boost::replace_all(format, Hour, std::to_string(hours));
    boost::replace_all(format, Minute, std::to_string(minutes));
    boost::replace_all(format, Seconds, std::to_string(seconds));

    return format;
}
