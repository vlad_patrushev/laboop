#include "Date.h"
#include <gtest/gtest.h>

TEST(Date, Constructors){
    Date t1(2,Jan,0);
    Date t2(2,Jan,0,0,0,0);
    ASSERT_TRUE(t1==t2);
}
TEST(Date,addSomething){
    Date t1(2010,May,13,12,42,53);
    ASSERT_TRUE(t1.addYears(14).getYears()==2024);
    ASSERT_TRUE(t1.addMonths(2).getMonths()==Jul);
    ASSERT_TRUE(t1.addDays(14).getDays()==27);
    ASSERT_TRUE(t1.addHours(12).getHours()==0);
    ASSERT_TRUE(t1.addMinutes(35).getMinutes()==17);
    ASSERT_TRUE(t1.addSeconds(7).getSeconds()==0);


}
TEST(Date,NormalizationOfDate){
    Date t1(9999,May,1222,500,344,544);
    Date t1_1(9999, Dec, 31, 23, 59, 59);
    Date t2(0,May,13,12,42,53);
    Date t2_2(1, Jan, 1, 0, 0, 0);
    ASSERT_LE(t1.getSeconds(),60);
    ASSERT_LE(t1.getMinutes(),60);
    ASSERT_LE(t1.getHours(),60);
    ASSERT_LE(t1.getDays(),31);
    ASSERT_LE(t1.getMonths(),12);

    ASSERT_TRUE(t1==t1_1);
    ASSERT_TRUE(t2==t2_2);

}
TEST(Date,getInterval){
    Date t1(2018,Jan,19,12,32,32);
    Date t2(2019,Jan,20,12,35,32);

    ASSERT_TRUE(t1.getInterval(t2).getYearsInterval()==1);
    ASSERT_TRUE(t1.getInterval(t2).getMonthsInterval()==0);
    ASSERT_TRUE(t1.getInterval(t2).getDaysInterval()==1);
    ASSERT_TRUE(t1.getInterval(t2).getHoursInterval()==0);
    ASSERT_TRUE(t1.getInterval(t2).getMinutesInterval()==3);
    ASSERT_TRUE(t1.getInterval(t2).getSecondsInterval()==0);


}
TEST(Date,addInterval){
    Date t1(2012,Feb,26,12,14,33);
    DateInterval t2(0,0,4,0,0,0);
    Date t3(2012,Mar,1,12,14,33);
    ASSERT_TRUE(t3==t1.addInterval(t2));

}
TEST(Date,formatDate){
    Date t0(2012,Feb,26,12,14,33);
    std::string t1="YYYY-MM-DD hh:mm:ss";
    std::string t2="YYYY-MMM-DD hh:mm:ss";
    std::string t3="YYYY something text MMM-DD hh something mm text:ss";
    std::string t1_1="2012-2-26 12:14:33";
    std::string t2_1="2012-Feb-26 12:14:33";
    std::string t3_1="2012 something text Feb-26 12 something 14 text:33";
    ASSERT_EQ(t0.formatDate(t1),t1_1);
    ASSERT_EQ(t0.formatDate(t2),t2_1);
    ASSERT_EQ(t0.formatDate(t3),t3_1);
}

