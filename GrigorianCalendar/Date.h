#pragma once

#include <boost/algorithm/string/replace.hpp>
#include<ctime>
#include<iostream>
#include <exception>
#include<string>
#include "DateInterval.h"
#include <map>

class DateInterval;

enum Month {
    Jan = 1, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
};

static std::map<const Month, const std::string> Convert{
        {Jan, "Jan"},
        {Feb, "Feb"},
        {Mar, "Mar"},
        {Apr, "Apr"},
        {May, "May"},
        {Jun, "Jun"},
        {Jul, "Jul"},
        {Aug, "Aug"},
        {Sep, "Sep"},
        {Oct, "Oct"},
        {Nov, "Nov"},
        {Dec, "Dec"}
}  ;

class Date {

public:
    Date();

    Date(const Date &obj);

    Date(unsigned int year, Month month, unsigned int day, unsigned int hour, unsigned int minute, unsigned int second);

    Date(unsigned int year, Month month, unsigned int day);

    Date(unsigned int hrs, unsigned int min, unsigned int sec);

    Date &operator=(const Date &right) {
        if (this == &right) {
            return *this;
        }
        years = right.years;
        currentMonths = right.currentMonths;
        days = right.days;
        hours = right.hours;
        minutes = right.minutes;
        seconds = right.seconds;
        return *this;
    }

    const bool operator==(const Date& right) {
        if ((years == right.years) && (currentMonths == right.currentMonths) && (days == right.days) &&
            (hours == right.hours) && (minutes == right.minutes) && (seconds == right.seconds)){
            return true;
        }
        return false;
    }

    static unsigned int convertFromEnum(Month month);

    static Month convertToEnum(unsigned int intMon);

    //additional constructors
    Date addYears(int years)const;

    Date addMonths(int months)const;

    Date addDays(int days)const;

    Date addHours(int hours)const;

    Date addMinutes(int minutes)const;

    Date addSeconds(int seconds)const;


    unsigned int getYears() const;

    unsigned int getMonths() const;

    unsigned int getDays() const;

    unsigned int getHours() const;

    unsigned int getMinutes() const;

    unsigned int getSeconds() const;

    std::string toString() {
        std::string date = std::to_string(this->years) + "-" + std::to_string(this->currentMonths) + "-" +
                           std::to_string(this->days);
        std::string time =
                std::to_string(this->hours) + ":" + std::to_string(this->minutes) + ":" + std::to_string(this->seconds);
        return date + " " + time;
    }

    DateInterval getInterval(const Date &another) const;

    Date addInterval(const DateInterval &another) const;

    std::string formatDate(std::string format);
private:

    unsigned int years;
    unsigned int months[12]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    Month currentMonths;
    unsigned int days;
    unsigned int hours;
    unsigned int minutes;
    unsigned int seconds;

    Date NormalizationOfDate();
};
