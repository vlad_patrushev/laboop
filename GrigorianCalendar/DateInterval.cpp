#include"DateInterval.h"
DateInterval::DateInterval() {
    yearsInterval=0;
    monthsInterval=0;
    daysInterval=0;
    hoursInterval=0;
    minutesInterval=0;
    secondsInterval=0;

}
DateInterval::DateInterval(int yearsInterval, int monthInterval, int daysInterval, int hoursInterval,
                           int minutesInterval, int secondsInterval)
{
    this->yearsInterval=yearsInterval;
    this->monthsInterval=monthInterval;
    this->daysInterval=daysInterval;
    this->hoursInterval=hoursInterval;
    this->minutesInterval=minutesInterval;
    this->secondsInterval=secondsInterval;

}
DateInterval::DateInterval(const DateInterval &obj){
    (*this)=obj;
}


int DateInterval::getYearsInterval() const{
    return yearsInterval;
}

int DateInterval::getMonthsInterval() const{
    return monthsInterval;
}

int DateInterval::getDaysInterval() const{
    return daysInterval;
}

int DateInterval::getHoursInterval() const{
    return hoursInterval;
}
int DateInterval::getMinutesInterval() const{
    return minutesInterval;
}
int DateInterval::getSecondsInterval() const{
    return secondsInterval;
}


void DateInterval::setYearsInterval(int yearsInterval){
    this->yearsInterval=yearsInterval;
}
void DateInterval::setMonthsInterval(int monthInterval){
    this->monthsInterval=monthInterval;
}
void DateInterval::setDaysInterval(int daysInterval){
    this->daysInterval=daysInterval;
}
void DateInterval::setHoursInterval(int hoursInterval){
    this->hoursInterval=hoursInterval;
}
void DateInterval::setMinutesInterval(int minutesInterval){
    this->minutesInterval=minutesInterval;
}
void DateInterval::setSecondsInterval(int secondsInterval){
    this->secondsInterval=secondsInterval;
}

