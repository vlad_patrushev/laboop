cmake_minimum_required(VERSION 3.8)
project(GrigorianCalendar)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "-pg")
# Locate GTest
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

set(SOURCE_FILES main.cpp Date.cpp DateInterval.cpp Date.h DateInterval.h DateTests.cpp)
add_executable(GrigorianCalendar ${SOURCE_FILES})
target_link_libraries(GrigorianCalendar ${GTEST_LIBRARIES} pthread)
#pthread ??