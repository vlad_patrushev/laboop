#pragma once

#include"Date.h"

class DateInterval {

public:
    DateInterval();

    DateInterval(int yearsInterval, int monthInterval, int daysInterval, int hoursInterval, int minutesInterval,
                 int secondsInterval);

    DateInterval(const DateInterval &obj);

    DateInterval &operator=(const DateInterval &right) {
        if (this == &right) {
            return *this;
        }
        yearsInterval = right.yearsInterval;
        monthsInterval = right.monthsInterval;
        daysInterval = right.daysInterval;
        hoursInterval = right.hoursInterval;
        minutesInterval = right.minutesInterval;
        secondsInterval = right.secondsInterval;
    }

    int getYearsInterval() const;

    int getMonthsInterval() const;

    int getDaysInterval() const;

    int getHoursInterval() const;

    int getMinutesInterval() const;

    int getSecondsInterval() const;

    void setYearsInterval(int yearsInterval);

    void setMonthsInterval(int monthInterval);

    void setDaysInterval(int daysInterval);

    void setHoursInterval(int hoursInterval);

    void setMinutesInterval(int minutesInterval);

    void setSecondsInterval(int secondsInterval);

private:
    int yearsInterval;
    int monthsInterval;
    int daysInterval;
    int hoursInterval;
    int minutesInterval;
    int secondsInterval;
};

