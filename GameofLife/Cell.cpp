//
// Created by vlad on 13/12/17.
//
#define GetIndex(index,size) ( ( ((index)%(size)) + (size))%(size) )// Замкнутость(0..9)
#include "Cell.h"

Cell::Cell( int X, int Y,bool life) {
    this->X=X;
    this->Y=Y;
    this->Life= life;
    neighbors.insert(std::pair<const int,const int>( GetIndex(X+1,SIZE_X),GetIndex(Y+1,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X+1,SIZE_X),GetIndex(Y,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X+1,SIZE_X),GetIndex(Y-1,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X,SIZE_X),GetIndex(Y+1,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X,SIZE_X),GetIndex(Y-1,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X-1,SIZE_X),GetIndex(Y+1,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X-1,SIZE_X),GetIndex(Y,SIZE_Y)));
    neighbors.insert(std::pair<const int,const int>( GetIndex(X-1,SIZE_X),GetIndex(Y-1,SIZE_Y)));
}



