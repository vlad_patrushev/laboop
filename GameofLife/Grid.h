//
// Created by vlad on 13/12/17.
//

#ifndef GAMEOFLIFE_GRID_H
#define GAMEOFLIFE_GRID_H


class Cell;//????????????
#include "Cell.h"
#include <vector>
class GameManager;
class Grid {

    public:

        int getAlive();
        bool isLive(int X,int Y);
        bool getIsUpdate();
        const int getWidth();
        const int getHight();
        Grid();
        void Update();
        void SetGrid(int X,int Y,bool life);
    private:
        std::vector<std::vector<Cell>> grid{hight,std::vector<Cell>{width}};
        int alive;
        bool isUpdate;
        static const int width=10;
        static const  int hight=10;
        //Подсчитывет количество живых соседей данной клетки
        int CountAliveNeighbours(Cell& cell);

};


#endif //GAMEOFLIFE_GRID_H
