//
// Created by vlad on 13/12/17.
//

#ifndef GAMEOFLIFE_VIEWER_H
#define GAMEOFLIFE_VIEWER_H

#include <iostream>
#include "Grid.h"
#include "GameManager.h"
class Viewer {
public:
    void print_result(Grid &grid);
};


#endif //GAMEOFLIFE_VIEWER_H
