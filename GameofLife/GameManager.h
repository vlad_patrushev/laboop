//
// Created by vlad on 13/12/17.
//

#ifndef GAMEOFLIFE_GAMEMANAGER_H
#define GAMEOFLIFE_GAMEMANAGER_H


#include "Grid.h"
#include "Comand.h"
#include "Parser.h"
#include <iostream>
#include <stack>
#include "Viewer.h"
class Grid;
class Viewer;
class Parser;
//Этот класс отвечает за состояния игры,выполнение команд, и за выход из неё;
class GameManager {
public:
    // проверяет на конец. Если да, то очищает поле и выводит gameover
    GameManager();
    static unsigned int getStepCount();
    void isStart();
    bool isEnd();
    void setPopStack();
    static void setPushStack(Grid* obj) {
        GameManager::OldStates.push(obj);
        int u =0;
    };
    // выполнение команды(передаётся имя команды и соответсвующему имени сопоставляется дейтсвие )
    void Execute(Parser& obj);
    //Добавляем в стек новый объект
    //friend void Grid::SetOldStates(Grid* obj);

    bool toBool(char arg);
    static std::stack<Grid*>OldStates ;

    static unsigned int StepCount;
    //Храним состояния игр
};

#endif //GAMEOFLIFE_GAMEMANAGER_H
