//
// Created by vlad on 13/12/17.
//

#ifndef GAMEOFLIFE_COMAND_H
#define GAMEOFLIFE_COMAND_H

#include <string>
#include "Grid.h"
#include "GameManager.h"
class GameManager;
class Grid;
//std::multimap<char,int,Cell*> &grid
// set XY (где X – от A до J, Y от 0 до 9) — установить организм в клетку
class CommandSet{
    public:
        void Execute( Grid &obj,int X, int Y);//grid- map <X,Y>

};
// reset – очистить поле и счетчик ходов
class CommandReset{
    public:
        void Execute(GameManager &obj);

};
// clear XY – очистить клетку
class CommandClear{
    public:
        void Execute(Grid &obj, int X, int Y);

};
// step N – прокрутить игру вперед на N шагов (может отсутствовать, тогда
//полагается равным 1)
class CommandStep{
    public:
        void Execute(Grid & obj, int arg);
};
// back – прокрутить игру назад на один шаг
class CommandBack{
    public:
        void Execute(GameManager &obj);
};
//save “filename” – сохранить состояние поля игры в текстовый файл в текущей
//директории (без истории ходов)
class CommandSave{
    public:
        void Execute(Grid &obj, std::string arg);
};
//load “filename” – загрузить состояние поля игры из текстового файла в текущей
//директории (с обнулением счетчика ходов)
class CommandLoad{
    public:
        void Execute(GameManager &obj,std::string arg);

};

#endif //GAMEOFLIFE_COMAND_H
