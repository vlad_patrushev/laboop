//
// Created by vlad on 13/12/17.
//
//TODO
//Разобраться с индексами
#include "Comand.h"
#include <fstream>

void CommandSet::Execute(Grid &obj, int X, int Y) {
    obj.SetGrid(X,Y, true);
}
void CommandBack::Execute(GameManager &obj) {
    obj.setPopStack();

}
void CommandClear::Execute(Grid &obj, int X, int Y) {
    obj.SetGrid(X,Y, false);

}
void CommandReset::Execute(GameManager &obj) {
    auto newGrid= new Grid();
    obj.setPushStack(newGrid);

}
void CommandStep::Execute(Grid &obj, int arg) {
    for(auto i=0;i<arg;i++){
        obj.Update();
    }

}
void CommandSave::Execute(Grid &obj, std::string arg) {
    // хранить последовательность 0 и 1
    std::ofstream fout;
    fout.open(arg);
    for(auto i=0;i<obj.getHight();i++){
        for(auto j=0;j<obj.getWidth();j++){
            fout<<obj.isLive(i,j);
        }
    }
    fout.close();

}
void CommandLoad::Execute(GameManager &obj, std::string arg) {
    std::ifstream fin;
    fin.open(arg);
    auto newGrid= new Grid();
    char buff[100];
    fin>>buff;
    fin.close();
    for(auto i=0;i<newGrid->getHight();i++){
        for(auto j=0;j<newGrid->getWidth();j++){
            newGrid->SetGrid(i,j,obj.toBool(buff[10*i+j]));
        }
    }
    obj.setPushStack(newGrid);
}