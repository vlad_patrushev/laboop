//
// Created by vlad on 13/12/17.
//

#ifndef GAMEOFLIFE_CELL_H
#define GAMEOFLIFE_CELL_H

#include <map>

class Cell {
    public:
    Cell() {

    };
    Cell( int X, int Y,bool life);
    int X;
    int Y;
    bool Life;// 1- live 0 -dead
    // координаты соседей клетки
    std::multimap<const int,const int> neighbors;

    private:
    int SIZE_X=10;
    int SIZE_Y=10;
};
#endif //GAMEOFLIFE_CELL_H
