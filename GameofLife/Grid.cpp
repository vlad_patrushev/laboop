//
// Created by vlad on 13/12/17.
//
#include "GameManager.h"
#include "Cell.h"

bool Grid::isLive(int X, int Y) {
    if(grid[X][Y].Life){
        return true;
    }
    return false;
}
const int Grid::getHight() {
    return hight;
}
const int Grid::getWidth() {
    return width;
}
int Grid::getAlive() {
    return alive;
}
bool Grid::getIsUpdate() {
    return isUpdate;
}

Grid::Grid() {
    for(char i=0;i<=9;i++){
        for(int j=0;j<=9;j++){
            Cell obj(i,j,false);
            grid[i][j]= obj;
        }
    }
    alive=0;//ошb,rf
    isUpdate= false;
}
void Grid::SetGrid(int X, int Y,bool life) {
    if((grid[X][Y].Life)&&(life)){
        return;
    }
    grid[X][Y].Life=life;

    if(life==true){
        alive++;
    }
    isUpdate= true;
}
//пробегаем по всем соседям и если есть живые, то считаем
int Grid::CountAliveNeighbours(Cell &cell) {
    int count=0;
    for(auto& item:cell.neighbors){
        if(grid[item.first][item.second].Life){
            count++;
        }
    }
    return count;

}
//Обновляет состояние и записывает в стек
void Grid::Update() {
    Grid* newGrid= new Grid();
    for(int i=0;i<hight;i++){
        for(int j=0;j<width;j++){
        if(CountAliveNeighbours(grid[i][j])==3) {

            for (auto &item:grid[i][j].neighbors) {
                //копируем соседей из одного поля в другое
                // проверяме чтобы не задеть соседа из нового состояния
                if((!newGrid->grid[item.first][item.second].Life) &&(!grid[item.first][item.second].Life))
                    newGrid->grid[item.first][item.second].Life = grid[item.first][item.second].Life;
            }
             newGrid->grid[i][j].Life = true;
            if (newGrid->grid[i][j].Life != this->grid[i][j].Life) {
                newGrid->isUpdate = true;
            }
            //j++;// для того чтобы перепрыгнуть через соседа( с ним и так всё ясно)
            continue;

        }
            if((CountAliveNeighbours(grid[i][j])==2)){
                newGrid->grid[i][j].Life = grid[i][j].Life;
            }
            else if(((CountAliveNeighbours(grid[i][j])<2)||(CountAliveNeighbours(grid[i][j])>3))&&(!newGrid->grid[i][j].Life)){
                newGrid->grid[i][j].Life = false;
            }
            if(newGrid->grid[i][j].Life!=this->grid[i][j].Life){
                newGrid->isUpdate=true;
            }
        }
    }
    for(int i=0;i<hight;i++){
        for(int j=0;j<width;j++){
                if(newGrid->grid[i][j].Life)
                    newGrid->alive++;
        }
    }

    GameManager::setPushStack(newGrid);
}