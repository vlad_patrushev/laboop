//
// Created by vlad on 16/12/17.
//

#ifndef GAMEOFLIFE_PARSER_H
#define GAMEOFLIFE_PARSER_H

#include <string>
#include <map>
class Parser {
    public:
        //getters
        Parser();
        std::string getCommandName();
        std::string getStringDate();
        int getFirstArg();
        int getSecondArg();

        void TakeCommand();
        void CommandParse();

    private:
    std::string CommandName;
    std::string StringDate;
    int  FirstArg;
    int  SecondArg;
    std::string NotParseCommand;
    // <название команды, количество аргументов, макс длина команды >
    std::multimap<const char*,unsigned int> ListOfCommand={{"save",4},{"load",4},
                                                          {"reset",5},{"back",4},
                                                          {"step",4},{"step",4},
                                                          {"clear",5},{"set",3}};
};


#endif //GAMEOFLIFE_PARSER_H
