//
// Created by vlad on 13/12/17.
//
#include "GameManager.h"
GameManager::GameManager() {
        StepCount=0;

        GameManager::OldStates.push(new Grid());
}
bool GameManager::toBool(char arg) {
    if(arg=='0'){
        return false;
    }
    if(arg=='1'){
        return true;

    //выкидываем эксепшн

    }
}
void GameManager::setPopStack()  {
    this->OldStates.pop();
}

unsigned int GameManager::getStepCount() {
    return StepCount;
}
bool GameManager::isEnd() {
    if((OldStates.top()->getAlive()==0)||(!GameManager::OldStates.top()->getIsUpdate())){
        return true;
    }
    return false;
}
//Основное управление
void GameManager::isStart() {
    while (true) {
        Parser newCommand;
        newCommand.TakeCommand();
        Execute(newCommand);
        Viewer screen;
        screen.print_result(*OldStates.top());
        //Отобразить текущее состояние поля
        if (isEnd()&&(newCommand.getCommandName()!="reset")) {
            std::cout << "GAME OVER!" << std::endl;
            break;
        }
    }
}
//Создаёт объекты соответствующих команд, которые уже выполняются
void GameManager::Execute(Parser &obj) {
        if(obj.getCommandName()=="step"){
            CommandStep Step;
                Step.Execute(*OldStates.top(),obj.getFirstArg());
                StepCount=StepCount+obj.getFirstArg();
         }
        if(obj.getCommandName()=="set"){
            CommandSet Set;
            Set.Execute(*OldStates.top(),obj.getFirstArg(),obj.getSecondArg());
        }
        if(obj.getCommandName()=="clear"){
            CommandClear Clear;
            Clear.Execute(*OldStates.top(),obj.getFirstArg(),obj.getSecondArg());
        }

        if(obj.getCommandName()=="back"){
            CommandBack Back;
                Back.Execute(*this);
                StepCount--;
            //Ексепшн если 0 шагов и возвращ назад
        }
        if(obj.getCommandName()=="reset"){
            CommandReset Reset;
                Reset.Execute(*this);
                StepCount=0;
        }

        if(obj.getCommandName()=="save"){
            CommandSave Save;
                Save.Execute(*OldStates.top(),obj.getStringDate());
        }
        if(obj.getCommandName()=="load"){
            CommandLoad Load;
                Load.Execute(*this,obj.getStringDate());
                StepCount=0;
        }
}


