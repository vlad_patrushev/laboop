//
// Created by vlad on 13/12/17.
//

#include"Viewer.h"
void Viewer::print_result(Grid &grid) {
    std::cout<<"\n\n\n\n\n  0 1 2 3 4 5 6 7 8 9\n";
    for(int i = 0; i < 10; i++)
    {   char ch='A'+i;
        std::cout<< ch <<" ";
        for(int j = 0; j < 10; j++)
        {
            if(grid.isLive(i,j) == true)
            {
                std::cout << "* ";
            }
            else
            {
                std::cout << ". ";
            }
            if(j == 9)
            {
                std::cout << std:: endl;
            }
        }
    }
    std::cout<<"Step:" <<GameManager::getStepCount()<<std::endl;
}